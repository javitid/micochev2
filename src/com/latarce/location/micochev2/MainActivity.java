package com.latarce.location.micochev2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.ads.*;


public class MainActivity extends FragmentActivity implements OnMapLongClickListener, OnClickListener, OnMyLocationChangeListener {

	private static final boolean IS_PREMIUM = false;
	private static final int DEFAULT_ZOOM_LEVEL_CAR = 15;
	private static final String POSITION_SAVED = "positionSaved";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String COMMENT = "comment";
	private static int MAP_BOTTOM_PADDING = 0;	//Premium = 0, No premium = 70 para el banner de Ads
	
	private CheckNews checkNews;
	
	private LatLng COCHE;
	private GoogleMap mapa;
	private Geocoder geocoder;
	private Location cocheLocation;
	private AdView adView;
	private CustomInfoWindowAdapter customInfoWindowAdapter;
	
	private Button btCar;
	private Button btGoToCar;
	private ToggleButton btSatellite;
	private Button btSave;
	private Button btInfo;
	private LinearLayout adsLayout;
	
	private SensorEventListener mSensorListener;
	private SensorManager mSensorManager;
	private Marker myPositionMarker;
	private MarkerOptions myPositionOptionsMarker;
	
	//Guardar posici�n al salir de la app y recuperarla al entrar
	private SharedPreferences positionSaved;
	private SharedPreferences.Editor editor;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		//COMPROBAR CONEXION DE DATOS
		if (!checkConnectionStatus())
        	finish();
		
		//CHECKEAR NOTICIAS
		checkNews = new CheckNews(this);
		checkNews.executeAsyncNews();
		
		//PREFERENCIAS -> Leer posicion guardada
		positionSaved = getSharedPreferences(POSITION_SAVED, MODE_PRIVATE);
		editor = positionSaved.edit();

		// Location del coche
		cocheLocation = new Location("coche");
		
		//BOTONES
		btCar = (Button)this.findViewById(R.id.btCar);
		btCar.setOnClickListener(this);
		btSatellite = (ToggleButton)this.findViewById(R.id.btSatellite);
		btSatellite.setOnClickListener(this);
		btSave = (Button)this.findViewById(R.id.btSave);
		btSave.setOnClickListener(this);
		btInfo = (Button)this.findViewById(R.id.btInfo);
		btInfo.setOnClickListener(this);
		btGoToCar = (Button)this.findViewById(R.id.btGoToCar);
		btGoToCar.setOnClickListener(this);
		
		
		//COMPROBAR SI ES LA VERISON PREMIUM
		if (IS_PREMIUM){
			//adView.setVisibility(View.INVISIBLE);
			//MAP_BOTTOM_PADDING = 0;
		}
		else{
			MAP_BOTTOM_PADDING = 100;

			//LAYOUT DE PUBLICIDAD
			adsLayout = (LinearLayout)findViewById(R.id.adsLayout);
			
			//ADVIEW
		    adView = new AdView(this);
		    adView.setAdUnitId("ca-app-pub-6720141601381474/4670795622");
		    adView.setAdSize(AdSize.SMART_BANNER);
		 
		    // Create an ad request.
	        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
	        
	        // Optionally populate the ad request builder.
	        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);	// Todos los emuladores
	        adRequestBuilder.addTestDevice("FBF6D5B3746AF6B129B3C9F6A3EF2CF8");	// Mi Nexus 4
	        
	        // Add the AdView to the view hierarchy.
	        adsLayout.addView(adView);
	        
	        // Start loading the ad.
	        adView.loadAd(adRequestBuilder.build());
	        
	        
	        //MOVER LOS BOTONES DE LA PARTE INFERIOR PARA DEJAR SITIO AL BANNER DE PUBLICIDAD
			LayoutParams paramsBtSatellite = (LayoutParams) btSatellite.getLayoutParams();
			paramsBtSatellite.setMargins(0, 0, 0, MAP_BOTTOM_PADDING);
			btSatellite.setLayoutParams(paramsBtSatellite);

			LayoutParams paramsBtInfo = (LayoutParams) btInfo.getLayoutParams();
			paramsBtInfo.setMargins(0, 0, 0, MAP_BOTTOM_PADDING);
			btInfo.setLayoutParams(paramsBtInfo);
		}

		//CUSTOM INFO WINDOW ADAPTER
		customInfoWindowAdapter = new CustomInfoWindowAdapter(getLayoutInflater());
		
		//MAPA
		mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa)).getMap();
		btSatellite.setChecked(false);
		mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		mapa.setMyLocationEnabled(true);
		mapa.getUiSettings().setZoomControlsEnabled(true);
		mapa.getUiSettings().setCompassEnabled(true);
		mapa.getUiSettings().setMyLocationButtonEnabled(true);
		mapa.setIndoorEnabled(true);
		mapa.setPadding(0, 0, 0, MAP_BOTTOM_PADDING);
		posicionarCoche();
		dibujarCoche();
		moverCamara();
		mapa.setOnMapLongClickListener(this);
		mapa.setOnMyLocationChangeListener(this);
		mapa.setInfoWindowAdapter(customInfoWindowAdapter);
		mapa.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
	        @Override
	        public void onInfoWindowClick(Marker marker) {
	        	editarComentarios();   
	        }
	    });
		
		//MARCADOR DE LAS OPCIONES DE MI POSICION
		myPositionOptionsMarker = new MarkerOptions()
		.position(new LatLng(0, 0))
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow))
		.anchor(0.5f, 0.5f)
		.position(new LatLng(0,0))
		.flat(true);
		
		//GEOCODER
		geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
		
	}
	

	// BOTONES
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
			case R.id.btCar:
				moveCameraToCar();
				break;
			case R.id.btGoToCar:
				Location myLoc = mapa.getMyLocation();
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+ myLoc.getLatitude() +","+ myLoc.getLongitude() +"&daddr="+ COCHE.latitude +"," + COCHE.longitude + "&t=m&dirflg=w"));
				i.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
				startActivity(i);
				break;
			case R.id.btSatellite:
				setMapaType();
		        break;
			case R.id.btSave:
				guardarPosicion((float)mapa.getMyLocation().getLatitude(), (float)mapa.getMyLocation().getLongitude());
				break;
			case R.id.btInfo:
				openOptionsMenu();
				break;
		}
	}
	
	
	//MENU
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.menu_position:
				Intent j = new Intent(this, Position.class);
				j.putExtra("position", getTextoPosicion());
				startActivity(j);
				break;
			case R.id.menu_share:
				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				//set the type  
				shareIntent.setType("text/plain");  
				//add a subject  
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_subject));
				//build the body of the message to be shared  
				String shareMessage = getResources().getString(R.string.share_message);  
				//add the message  
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);  
				//start the chooser for sharing  
				startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.share_chooser)));
				break;
			case R.id.menu_info:
				Intent l = new Intent(this, About.class);
				startActivity(l);
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	// ESTADOS
	@Override
	protected void onResume(){
		mapa.setMyLocationEnabled(true);
		mapa.getUiSettings().setCompassEnabled(true);
		setMapaType();
		super.onResume();
		if (!IS_PREMIUM){
			adView.resume();
		}
		
		//SENSOR MAGNETICO
		mSensorListener = new SensorEventListener() {
		    @Override
		    public void onSensorChanged(SensorEvent event) {
		        float mOrientation = event.values[0];
		        drawOrientation(mOrientation);
		    }

			@Override
			public void onAccuracyChanged(Sensor arg0, int arg1) {
				// TODO Auto-generated method stub
			}
		};
		setupSensorManager();
	}
	
	@Override
	protected void onPause(){
		mapa.setMyLocationEnabled(false);
		mapa.getUiSettings().setCompassEnabled(false);
		super.onPause();
		if (!IS_PREMIUM){
			adView.pause();
		}
		mSensorManager.unregisterListener(mSensorListener);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (!IS_PREMIUM){
			adView.destroy();
		}
	}

	
	// GUARDAR POSICION
	public void guardarPosicion(float latitude, float longitude){
		if (mapa.getMyLocation() != null){
			editor.putFloat(LATITUDE, latitude);
			editor.putFloat(LONGITUDE, longitude);
			editor.commit();
			posicionarCoche();
			dibujarCoche();
			moverCamara();
			editarComentarios();
		}
	}
	
	
	public void posicionarCoche() {
		COCHE = new LatLng(positionSaved.getFloat(LATITUDE, 0), positionSaved.getFloat(LONGITUDE, 0));
		cocheLocation.setLatitude(positionSaved.getFloat(LATITUDE, 0));
		cocheLocation.setLongitude(positionSaved.getFloat(LONGITUDE, 0));
	}
	
	public void dibujarCoche() {
		mapa.clear();
		mapa.addMarker(new MarkerOptions()
		.position(COCHE)
		//.title("Coche")
		//.snippet(getTextoDistancia())
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.coche))
		.anchor(0.3f, 1.0f));
		
		// Enviar la informaci�n
		customInfoWindowAdapter.setComment(positionSaved.getString(COMMENT, ""));
		customInfoWindowAdapter.setTextInfo(getTextoDistancia());
	}
	
	public void moverCamara(){
		mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(COCHE, DEFAULT_ZOOM_LEVEL_CAR));
	}
	
	
	// MOVER VISTA AL COCHE
	public void moveCameraToCar() {
		mapa.animateCamera(CameraUpdateFactory.newLatLng(COCHE));
	}

	
	// A�ADIR MARCADOR
	public void addMarker() {
		mapa.addMarker(new MarkerOptions().position(
		new LatLng(mapa.getCameraPosition().target.latitude,
		mapa.getCameraPosition().target.longitude)));
	}
	
	
	// CLICK SOBRE EL MAPA
	@Override
	public void onMapLongClick(final LatLng puntoPulsado) {
        
		List<Address> addressTouched = new ArrayList<Address>();
		String touchedTextInfo = "";
		try{
	    	// Obtener la direcci�n d�nde se ha pulsado
			addressTouched = geocoder.getFromLocation(puntoPulsado.latitude, puntoPulsado.longitude, 1);
			touchedTextInfo += "�Desea marcar la posici�n del coche en:\n"
	    						 + addressTouched.get(0).getAddressLine(0)
	    						 + "\n" + addressTouched.get(0).getAddressLine(1)
	    						 + "\n" + addressTouched.get(0).getAddressLine(2) + "?\n\n";
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
		
		// VENTANA DE CONFIRMACI�N DE PARQUING
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(touchedTextInfo);
		builder.setTitle("Guardar Posici�n");
		builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	    		guardarPosicion((float)puntoPulsado.latitude, (float)puntoPulsado.longitude);
	            dialog.dismiss();
	        }
	    });
		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	Toast.makeText(getBaseContext(), "No se ha guardado la nueva posici�n", Toast.LENGTH_SHORT).show();
	            dialog.dismiss();
	        }
	    });

		AlertDialog alert = builder.create();
	    alert.show();
	    
		//mapa.addMarker(new MarkerOptions().position(puntoPulsado)
		//	.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	}

	
	// CAMBIO DE POSICION
	@Override
	public void onMyLocationChange(Location location) {
		dibujarCoche();
		// Activar bot�n de ruta
    	btGoToCar.setVisibility(View.VISIBLE);
    	/* BEARING
		* //Convert LatLng to Location
		* Location carLocation = new Location("COCHE");
		* carLocation.setLatitude(COCHE.latitude);
		* carLocation.setLongitude(COCHE.longitude);
		* bearing = location.bearingTo(carLocation);
		* Log.d("Bearing ", Float.toString(bearing));
		*/
	}
	
	
	// INFORMACION DE LAS POSICIONES Y DISTANCIA ENTRE ELLAS
	private String getTextoPosicion() {
		String positionTextInfo = "";
		Location myLoc = mapa.getMyLocation();
		if(myLoc != null){
	    	List<Address> address = new ArrayList<Address>();
	    	List<Address> car_address = new ArrayList<Address>();
	    	
		    //GEOCODER
		    try{
		    	// Obtener la direcci�n del usuario
		    	address = geocoder.getFromLocation(myLoc.getLatitude(), myLoc.getLongitude(), 1);
		    	positionTextInfo = "Yo estoy en:\n"
		    						 + address.get(0).getAddressLine(0)
		    						 + "\n" + address.get(0).getAddressLine(1)
		    						 + "\n" + address.get(0).getAddressLine(2) + "\n\n";

		    	// Obtener la direcci�n del coche
		    	car_address = geocoder.getFromLocation(COCHE.latitude, COCHE.longitude, 1);
		    	positionTextInfo += "Mi coche esta en:\n"
		    						 + car_address.get(0).getAddressLine(0)
		    						 + "\n" + car_address.get(0).getAddressLine(1)
		    						 + "\n" + car_address.get(0).getAddressLine(2) + "\n\n";
		    	
		    	// Obtener la distancia entre ambos
		    	positionTextInfo += "El coche est� a " 
						+ (int)Math.rint(mapa.getMyLocation().distanceTo(cocheLocation)) 
						+ " metros de distancia";
		    		
		    }catch(Exception e) {
		    	e.printStackTrace();
		    }
		    
	    }
	    else{
	    	//positionTextInfo = getResources().getString(R.string.error_position);   	
	    	positionTextInfo = "No se ha podido obtener la posici�n actual";
	    }
		return positionTextInfo;
	}
	
	private String getTextoDistancia() {
		String distancia = getResources().getString(R.string.error_position);
		Location myLoc = mapa.getMyLocation();
		if(myLoc != null){
			distancia = "El coche est� a " 
					+ (int)Math.rint(mapa.getMyLocation().distanceTo(cocheLocation)) 
					+ " metros";
		}
		return distancia;
	}
	
	//CHECKEAR CONEXION
    // Check connection Status
    private boolean checkConnectionStatus(){
    	ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netInfo = conMan.getActiveNetworkInfo();
    	if (netInfo == null){
    		Toast.makeText(this.getBaseContext(), getResources().getString(R.string.error_connection) + "\n" + getResources().getString(R.string.error_gps), Toast.LENGTH_LONG).show();
    	    return false;
    	}
    	if(netInfo.isConnectedOrConnecting() == false){
			Toast.makeText(this.getBaseContext(), getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    	    return false;
    	}
    	return true;
    }
    
    
    //RESULTADO DE LA INSERCION DE COMENTARIOS
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if(resultCode == RESULT_OK){
				editor.putString(COMMENT, data.getStringExtra("comment"));
				editor.commit();
			}
			if (resultCode == RESULT_CANCELED) {    
				//Write your code if there's no result
			}
		}
	}
	
	//COMENTARIOS
	public void editarComentarios(){
		Intent i = new Intent(this, SetComment.class);
		i.putExtra(COMMENT, positionSaved.getString(COMMENT, ""));
	    startActivityForResult(i, 1);
	}
	
	//COMPROBAR QUE EL MAPA SE CORRESPONDE CON EL ESTADO DEL BOTON
	public void setMapaType(){
        if(btSatellite.isChecked())
        	mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        else
        	mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	}
	
	//INICIALIZAR SENSOR
	private void setupSensorManager() {
		mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
	    mSensorManager.registerListener(mSensorListener,
	            mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
	            SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	//PINTAR FLECHA DE ORIENTACION SOBRE EL MAPA
	private void drawOrientation(float angle) {
		Log.d("Angle ", Float.toString(angle));
		Location myLoc = mapa.getMyLocation();
		if (myLoc != null){
			if (myPositionMarker != null){
				myPositionMarker.remove();
			}
			myPositionMarker = mapa.addMarker(myPositionOptionsMarker);
			myPositionMarker.setPosition(new LatLng(myLoc.getLatitude(), myLoc.getLongitude()));
			myPositionMarker.setRotation(angle);
		}
	}
}
