package com.latarce.location.micochev2;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Base64;

public class CheckNews{
	private String LOADURL = "http://latarce.com/spl_news/check_novedades.php?action=get_novedades_coche";
	private String USER = "user";
	private String PWD = "pwd";
	private Context context;
	private AsyncNews asyncNews;
	
	// Constructor
	public CheckNews(Context context){
		this.context = context;
		this.asyncNews = new AsyncNews();
	}
	
	public void executeAsyncNews(){
		asyncNews.execute();
	}
	
	// Async task
	private class AsyncNews extends AsyncTask<Void, Void, Boolean> {
		private RestClient clientResponse;
		@Override
		protected Boolean doInBackground(Void... params) {
			clientResponse = loadServerMsgs();
			return true;
		}
		@Override
		protected void onPostExecute(final Boolean success) {
			printNovedadesIntent(clientResponse);
		}
		@Override
		protected void onCancelled() {
		}
	}
	
	
    // Load Server Messages - REST Service
    private RestClient loadServerMsgs(){  
	    RestClient client = new RestClient(LOADURL);
	    String authentication = USER + ":" + PWD;
    	String encoding = Base64.encodeToString(authentication.getBytes(), Base64.NO_WRAP);
    	client.AddHeader("Authorization", "Basic " + encoding);
	     
	    try {
	        //the actual call here
	        client.Execute(RestClient.RequestMethod.GET);
	    } catch (Exception e) {
	        e.printStackTrace();
	    } 
	    return client;		
    }
    
    
    // Get Status values
    private void printNovedadesIntent(RestClient client){
    	String response = client.getResponse();
    	if (response != null){
			// Add messages from JSON response
		    try {
		    	JSONObject jsonMessages = new JSONObject(response);
		    	JSONObject jsonStatus = jsonMessages.getJSONObject("novedades");	    	

		    	if (Integer.parseInt(jsonStatus.getString("v2")) == 1){
		    		//Toast.makeText(getBaseContext(), "Novedades en MiCocheV2", Toast.LENGTH_SHORT).show();
		    		Intent intent = new Intent(this.context, Novedades.class);
		    		intent.putExtra("message", jsonStatus.getString("v2_mensaje"));
		    		this.context.startActivity(intent);
		    	}

			} catch (JSONException e) {
				e.printStackTrace();
			}
    	}
    }
	
}