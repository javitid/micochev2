package com.latarce.location.micochev2;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements InfoWindowAdapter{
    private final View mymarkerview;
    private String textInfo;
    private String comment;

	CustomInfoWindowAdapter(LayoutInflater layoutInflater) {
        mymarkerview = layoutInflater.inflate(R.layout.custom_info_window, null); 
    }

	public void setTextInfo(String textInfo) {
		this.textInfo = textInfo;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
    public View getInfoWindow(Marker marker) {      
        render(marker, mymarkerview);
        return mymarkerview;
    }

    public View getInfoContents(Marker marker) {
       return null;
    }

    private void render(Marker marker, View view) {
    	TextView textView = (TextView)view.findViewById(R.id.textInfo);
    	textView.setText(textInfo + "\n\n" + view.getResources().getString(R.string.note) + ": " + comment);
    }
    
}
