package com.latarce.location.micochev2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SetComment extends Activity implements OnClickListener{
	
	private static final String COMMENT = "comment";
	private EditText commentEditText;
	private String comment;
	private Button btOK, btNOK;
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_comment);
		
		comment = getIntent().getStringExtra(COMMENT);
		commentEditText = (EditText)this.findViewById(R.id.comment);
		commentEditText.setText(comment);
		
		//BOTONES
		btOK = (Button)this.findViewById(R.id.btCommentOK);
		btOK.setOnClickListener(this);
		btNOK = (Button)this.findViewById(R.id.btCommentNOK);
		btNOK.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent returnIntent;
		switch (v.getId()) {
			case R.id.btCommentOK:
				// Devolver el estado actual del pedido para repintar la pantalla con el nuevo valor
				returnIntent = new Intent();
				returnIntent.putExtra("comment", commentEditText.getText().toString());
				setResult(RESULT_OK, returnIntent);     
				finish();
				break;
			case R.id.btCommentNOK:
				// Devolver el estado actual del pedido para repintar la pantalla con el nuevo valor
				returnIntent = new Intent();
				setResult(RESULT_CANCELED, returnIntent);     
				finish();
				break;
		}
		
	}

}
